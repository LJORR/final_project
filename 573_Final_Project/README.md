## Polar & Subtropical Jet Analysis

This project is an analysis and overview of the subtropical
and polar jet streams over a 50 year period. It features 3 
main sections: the first formatting the data, the second 
creating line plots of the data, and the third making a 
few plots using .nc file data. The Jupyter Notebook is 
meant to be stepped through and read like an informal lab 
report. All cells are important to the functionality of 
the notebook and should be ran in sequential order. A few 
cells make take a minute or two to run, but no cell should 
take any longer than, as an extreme, 5 minutes. In the off
chance that your data files have loaded in differently 
or you've saved them in a separate folder, the file path when 
loading in the data will need to be updated. This project 
should be ran in the aos573 environment, which was included 
in the project's git repo. 

The data for this project comes in two forms: 8 csv files and
1 nc file. The 8 csv files can be divided in half, 4 for the 
Polar Jet and 4 for the Subtropical Jet, each file representing 
a different variable for each jet. These four variables are: jet
average speed, average jet latitude, average latitude displacement,
and the core potential vorticity isertel of the jet. The nc file 
was previously a gempak file of polar jet data and contains lat/lon 
values so that plotting maps is now an option. 